<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Student;
use App\Http\Livewire\Admin\Category;
use App\Http\Livewire\Admin\Dashboard as AdminDashboard;
use App\Http\Livewire\Admin\Post;
use App\Http\Livewire\Author\Category as AuthorCategory;
use App\Http\Livewire\Author\Dashboard;
use App\Http\Livewire\Author\Post as AuthorPost;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
# Import controller here


Route::get("/", [Student::class, "welcome"]);
//Route::view('about-us', 'about-us');
Route::get("/about-us",[Student::class,"style"]);
// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function () {

//     $data = ["name" => "Sanjay", "email" => "sanjay@mail.com"];

//     return view('welcome', $data); // /resources/views/welcome.blade.php

// });

# At view file - welcome.blade.php

//{{ $name}} {{ $email }}
// # Route for welcome page
// Route::view("/", "welcome");

# Route with parameters
#Route::view("/", "welcome", ["name" => "vith", "email" => "vith@mail.com"]);
Route::get('/', function () {
    return view('frontend.home');
})->name('home');

Route::get('/posts', function () {
    return view('frontend.post_list');
})->name('front.posts');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//admin
Route::middleware(['auth:sanctum', 'verified','authadmin'])->group(function(){
   Route::get('/admin/dashboard',AdminDashboard::class)->name('admin.dashboard');
   Route::get('/admin/categories',Category::class)->name('admin.categories');
   Route::get('/admin/categories',Post::class)->name('admin.post');
});
//author
Route::middleware(['auth:sanctum', 'verified','authauthor'])->group(function(){
   Route::get('/author/dashboard',Dashboard::class)->name('author.dashboard');
   Route::get('/author/categories',AuthorCategory::class)->name('author.categories');
   Route::get('/author/categories',AuthorPost::class)->name('author.post');
});


