<header>
	<!-- Navbar top -->
	<div class="navbar-top d-none d-lg-block">
		<div class="container">
			<div class="row d-flex align-items-center my-2">
				<!-- Top bar left -->
				<div class="col-sm-8 d-sm-flex align-items-center">
					<!-- Title -->
					<div class="me-3">
						<span class="badge bg-primary p-2 px-3 text-white">Trending:</span>
					</div>
					<!-- Slider -->
					<div class="tiny-slider arrow-end arrow-xs arrow-bordered arrow-round arrow-md-none">
						<div class="tiny-slider-inner"
							data-autoplay="true"
							data-hoverpause="true"
							data-gutter="0"
							data-arrow="true"
							data-dots="false"
							data-items="1">
							<!-- Slider items -->
							<div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think</a></div>
							<div> <a href="#" class="text-reset btn-link">How the 10 worst business fails of all time could have been prevented </a></div>
							<div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think </a></div>
						</div>
					</div>
				</div>

				<!-- Top bar right -->
				<div class="col-sm-4">
					<ul class="list-inline mb-0 text-center text-sm-end">
						<li class="list-inline-item">
							<span>Wed, March 31, 2021</span>
						</li>
						<li class="list-inline-item">
							<i class="bi bi-cloud-hail text-info"></i>
							<span>13 °C NY, USA</span>
						</li>
					</ul>
				</div>
			</div>
			<!-- Divider -->
			<div class="border-bottom border-2 border-primary opacity-1"></div>
		</div>
	</div>
	<!-- Navbar logo section START -->
	<div>
		<div class="container">
			<div class="d-sm-flex justify-content-sm-between align-items-sm-center my-2">
				<!-- Logo START -->
				<a class="navbar-brand d-block" href="index.html">
					<img class="navbar-brand-item light-mode-item" src="assets/images/logo.svg" alt="logo" style="height: 46px;">
					<img class="navbar-brand-item dark-mode-item" src="assets/images/logo-light.svg" alt="logo" style="height: 46px;">
				</a>
				<!-- Logo END -->
				<!-- Adv -->
				<div>
					<a href="#" class="card-img-flash d-block">
						<img src="assets/images/adv-2.png" alt="adv">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Navbar logo section END -->

	<!-- Navbar START -->
	<div class="navbar-dark navbar-sticky header-static">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<div class="w-100 bg-dark d-flex">

					<!-- Responsive navbar toggler -->
					<button class="navbar-toggler me-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="text-muted h6 ps-3">Menu</span>
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- Main navbar START -->
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav navbar-nav-scroll navbar-lh-sm">


							<li class="nav-item">	<a class="nav-link" href="{{route('home')}}">Home</a></li>

                            <li class="nav-item">	<a class="nav-link" href="{{route('front.posts')}}">All Post</a></li>

							<li class="nav-item">	<a class="nav-link" href="#">កំសាន្ត</a></li>

							<li class="nav-item">	<a class="nav-link" href="#">បច្ចេកវិទ្យា</a></li>

							<li class="nav-item">	<a class="nav-link" href="#">ជីវិត​ និង សង្គម</a></li>

                            <li class="nav-item">	<a class="nav-link" href="#">កីឡា</a></li>

                            <li class="nav-item">	<a class="nav-link" href="#">កូវីដ-១៩</a></li>

							<li class="nav-item">	<a class="nav-link" href="#">សុខភាព</a></li>

						</ul>
					</div>
					<!-- Main navbar END -->

					<!-- Nav right START -->
					<div class="nav flex-nowrap align-items-center me-2">
						<!-- Dark mode switch -->
						<div class="nav-item">
							<div class="modeswitch" id="darkModeSwitch">
								<div class="switch"></div>
							</div>
						</div>
						<!-- Nav bookmark -->
						<div class="nav-item">
							<a class="nav-link" href="#">
								<i class="bi bi-bookmark-heart fs-4"></i>
							</a>
						</div>
						<!-- Nav user -->
						<div class="nav-item">
							<a class="nav-link" href="#">
								<i class="bi bi-person-square fs-4"></i>
							</a>
						</div>
						<!-- Nav Search -->
						<div class="nav-item dropdown nav-search dropdown-toggle-icon-none">
							<a class="nav-link text-uppercase dropdown-toggle" role="button" href="#" id="navSearch" data-bs-toggle="dropdown" aria-expanded="false">
								<i class="bi bi-search fs-4"> </i>
							</a>
							<div class="dropdown-menu dropdown-menu-end shadow rounded p-2" aria-labelledby="navSearch">
								<form class="input-group">
									<input class="form-control border-success" type="search" placeholder="Search" aria-label="Search">
									<button class="btn btn-success m-0" type="submit">Search</button>
								</form>
							</div>
						</div>
						<!-- Offcanvas menu toggler -->
						<div class="nav-item">
							<a class="nav-link pe-0" data-bs-toggle="offcanvas" href="#offcanvasMenu" role="button" aria-controls="offcanvasMenu">
								<i class="bi bi-text-right rtl-flip fs-2" data-bs-target="#offcanvasMenu"> </i>
							</a>
						</div>
					</div>
					<!-- Nav right END -->
				</div>
			</div>
		</nav>
	</div>
	<!-- Navbar END -->
</header>
