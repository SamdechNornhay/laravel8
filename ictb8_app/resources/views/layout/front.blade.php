<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from blogzine.webestica.com/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Aug 2021 05:17:53 GMT -->
<head>
	<title>Blogzine - Blog and Magazine Bootstrap 5 Theme</title>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Webestica.com">
	<meta name="description" content="Bootstrap based News, Magazine and Blog Theme">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com/">
	<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;700&amp;family=Rubik:wght@400;500;700&amp;display=swap" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="assets/vendor/font-awesome/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap-icons/bootstrap-icons.css">
	<link rel="stylesheet" type="text/css" href="assets/vendor/tiny-slider/tiny-slider.css">
	<link rel="stylesheet" type="text/css" href="assets/vendor/glightbox/css/glightbox.css">

	<!-- Theme CSS -->
	<link id="style-switch" rel="stylesheet" type="text/css" href="assets/css/style.css">

</head>

<body>

<!-- Offcanvas START -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasMenu">
  <div class="offcanvas-header">
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body d-flex flex-column pt-0">
		<div>
			<img class="light-mode-item my-3" src="assets/images/logo.svg" alt="logo">
			<img class="dark-mode-item my-3" src="assets/images/logo-light.svg" alt="logo">
			<p>The next-generation blog, news, and magazine theme for you to start sharing your stories today! </p>
			<!-- Nav START -->
			<ul class="nav d-block flex-column my-4">
				<li class="nav-item">
					<a href="index.html" class="nav-link h5 d-block">Home</a>
				<li class="nav-item">
					<a class="nav-link h5" href="#">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link h5" href="#">Our Journal</a>
				</li>
				<li class="nav-item">
					<a class="nav-link h5" href="#">Contact Us</a>
				</li>
			</ul>
			<!-- Nav END -->
			<div class="bg-primary-soft p-4 mb-4 text-center w-100 rounded">
				<span>The Blogzine</span>
				<h3>Save on Premium Membership</h3>
				<p>Get the insights report trusted by experts around the globe. Become a Member Today!</p>
				<a href="#" class="btn btn-warning">View pricing plans</a>
			</div>
		</div>
		<div class="mt-auto pb-3">
			<!-- Address -->
			<p class="text-body mb-2 fw-bold">New York, USA (HQ)</p>
			<address class="mb-0">750 Sing Sing Rd, Horseheads, NY, 14845</address>
			<p class="mb-2">Call: <a href="#" class="text-body"><u>469-537-2410</u> (Toll-free)</a> </p>
			<a href="#" class="text-body d-block">hello@blogzine.com</a>
		</div>
  </div>
</div>
<!-- Offcanvas END -->

<!-- =======================
Header START -->
@include('layout.partials.front_header')
<!-- =======================
Header END -->

<!-- **************** MAIN CONTENT START **************** -->
<main>
    @yield('main_content')
</main>
<!-- **************** MAIN CONTENT END **************** -->

<!-- =======================
Footer START -->
<footer class="bg-dark pt-5">

@include('layout.partials.front_footer')

	<!-- Footer copyright END -->
</footer>
<!-- =======================
Footer END -->


<!-- =======================
{{-- Cookies alert START -->
<div class="alert alert-dismissible fade show bg-dark text-white position-fixed start-0 bottom-0 z-index-99 shadow p-4 ms-3 mb-3 col-9 col-md-4 col-lg-3 col-xl-2" role="alert">
  This website stores cookies on your computer. To find out more about the cookies we use, see our <a class="text-white" href="#"> Privacy Policy</a>
  <div class="mt-4">
	  <button type="button" class="btn btn-success-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">Accept</span>
	  </button>
	  <button type="button" class="btn btn-danger-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">Decline</span>
	  </button>
	</div>
	<div class="position-absolute end-0 top-0 mt-n3 me-n3"><img class="w-100" src="assets/images/cookie.svg" alt="cookie"></div>
</div>
<!-- =======================
Cookies alert END --> --}}


Back to top -->
<div class="back-top"><i class="bi bi-arrow-up-short"></i></div>

<!-- =======================
JS libraries, plugins and custom scripts -->

<!-- Bootstrap JS -->
<script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<!-- Vendors -->
<script src="assets/vendor/tiny-slider/tiny-slider.js"></script>
<script src="assets/vendor/sticky-js/sticky.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.js"></script>

<!-- Template Functions -->
<script src="assets/js/functions.js"></script>

</body>

<!-- Mirrored from blogzine.webestica.com/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Aug 2021 05:17:58 GMT -->
</html>
