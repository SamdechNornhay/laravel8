@extends('layouts.app')

@section('title', 'About Us')

@section('sidebar')
    @parent <!-- Includes parent sidebar -->

    <p>About us page sidebar</p>
@stop

@section('content')

    @include('partials.slider')

    <p>This is about us content page.</p>
@stop
