@extends('layout.front')

@section('main_content')
<!-- =======================
Latest news slider START -->
@include('frontend.includes.latest_news')
<!-- =======================
Latest news slider END -->

<!-- =======================
Main hero START -->
@include('frontend.includes.main_hero')
<!-- =======================
Main hero END -->

<!-- =======================
Feature News slider START -->
@include('frontend.includes.feature_news')
<!-- =======================
Feature News slider END -->

<!-- =======================
Main content START -->
@include('frontend.includes.main-content')
<!-- =======================
Main content END -->

<!-- =======================
Featured video START -->
{{-- @include('frontend.includes.videos') --}}
<!-- =======================
Featured video END -->

<!-- =======================
Main content 2 START -->
@include('frontend.includes.main-content2')
<!-- =======================
Main content 2 END -->
@endsection
