<section class="py-4 card-grid">
	<div class="container">
		<div class="row">
			<div class="col">
				<!-- Slider START -->
				<div class="tiny-slider">
					<div class="tiny-slider-inner"
					data-autoplay="true"
					data-hoverpause="true"
					data-gutter="24"
					data-arrow="false"
					data-dots="false"
					data-items-md="2"
					data-items-sm="2"
					data-items-xs="1"
					data-items="3" >
                    @for ($i =1; $i <5; $i++)
						<!-- Card item START -->
						<div class="card">
							<div class="row g-3">
								<div class="col-3">
									<img class="rounded-3" src="assets/images/blog/1by1/0{{$i}}.jpg" alt="">
								</div>
								<div class="col-9">
									<h5><a href="post-single-5.html" class="btn-link stretched-link text-reset fw-bold">The pros and cons of business agency</a></h5>
									<!-- Card info -->
									<ul class="nav nav-divider align-items-center small">
										<li class="nav-item">
											<div class="nav-link position-relative">
												<span>by <a href="#" class="stretched-link text-reset btn-link">Samuel</a></span>
											</div>
										</li>
										<li class="nav-item">Jan 22, 2021</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- Card item END -->
					@endfor
                </div>
				</div>
			</div>
		</div> <!-- Row END -->
	</div>
</section>
