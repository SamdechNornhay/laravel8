<?php

namespace App\Http\Livewire\Author;

use Livewire\Component;

class Dashboard extends Component
{
    public function render()
    {
        return view('livewire.author.dashboard');
    }
}
