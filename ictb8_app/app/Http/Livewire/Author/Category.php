<?php

namespace App\Http\Livewire\Author;

use Livewire\Component;

class Category extends Component
{
    public function render()
    {
        return view('livewire.author.category');
    }
}
